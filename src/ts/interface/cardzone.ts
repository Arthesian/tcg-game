import { Card } from './../class/card'

export default interface iCardZone {
    Cards : Card[];
    GetNumberOfCards : () => number;
    AddCard : (card : Card) => this;
    RemoveCard : (card : Card) => this;
    Shuffle : () => this;
}