///<reference path="class/game.ts"/>
///<reference path="class/player.ts"/>

import Game from "./class/game";
import Player from "./class/player";



var GAME = new Game();

var player1 = new Player(GAME);
var player2 = new Player(GAME);

GAME.CreateGame([player1, player2], 20);