import Card from './../class/card';
import * as Enums from '../enums/enums';

/**
 * Card helper class comes with handy methods that are used throughout the game
 *
 * @class CardHelper
 */
class CardHelper {
    isType = (card : Card, cardType : Enums.CardType) => {
        return card.Types.some(t => t === cardType);
    }
    IsCreature = (card : Card) => {
        return card.Types.some(t => t === Enums.CardType.Creature);
    }
    IsEnchantment = (card : Card) => {
        return card.Types.some(t => t.toString().indexOf('Enchant') > -1);
    }
    IsInstant = (card : Card) => {
        return card.Types.some(t => t === Enums.CardType.Instant);
    }
    IsSorcery = (card : Card) => {
        return card.Types.some(t => t === Enums.CardType.Sorcery);
    }
    IsArtifact = (card : Card) => {
        return card.Types.some(t => t === Enums.CardType.Artifact);
    }
    IsLand = (card : Card) => {
        return card.Types.some(t => t === Enums.CardType.Land);
    }
    IsBasicLand = (card : Card) => {
        return this.IsLand(card) && card.Types.some(t => t === Enums.CardType.Basic);
    }
    IsColor = (card : Card, color : Enums.ManaType) => {
        return card.Colors.some(c => c == color);
    }
    HasSubType = (card : Card, subType : string) => {
        return card.SubTypes.some(t => t.indexOf(subType) > -1);
    }
}

export default new CardHelper();