import Card from './../class/card';
import CardHelper from './card';
import * as Enums from '../enums/enums';

/**
 * Cards helper provides multiple functions used throughout the whole game ( for sets of cards )
 *
 * @class CardsHelper
 */
class CardsHelper {

    Shuffle(cards : Card[]){
        var shuffledCards = cards.sort((a, b) => {
            return Math.round(Math.random());
        });

        return shuffledCards;
    }

    GetType(cards : Card[], cardType : Enums.CardType) {
        return cards.filter(c => CardHelper.isType(c, cardType));
    }

    GetSubType(cards : Card[], subType : string) {
        return cards.filter(c => CardHelper.HasSubType(c, subType));
    }

    GetBasicLandsOfType(cards : Card[], type : Enums.ManaType){
        if(type && type !== Enums.ManaType.Any as Enums.ManaType) {
            return cards.filter(c => CardHelper.IsBasicLand(c) && CardHelper.IsColor(c, type));
        }else {
            return cards.filter(c => CardHelper.IsBasicLand(c));
        }
    }

    GetMaxValue(cards : Card[], property : string) {
        var max = 0;
        cards.forEach(card => {
            card[property] > max ? max = card[property] : max;
        });
        return cards.length === 0 ? Number.NaN : max;
    }

    GetMinValue(cards : Card[], property : string){
        var min = Number.MAX_VALUE;
        cards.forEach(card => {
            card[property] < min ? min = card[property] : min;
        });
        return cards.length === 0 ? Number.NaN : min;
    }

    UnTap(cards : Card[], forced? : boolean) {
        return new Promise(resolve => {
            var untaps = [];

            cards.forEach(c => { 
                untaps.push(c.UnTap(forced)); 
            });

            Promise.all(untaps).then(cards => resolve(cards));
        });
    }

    GetUntapped(cards : Card[]) {
        return cards.filter(c => !c.IsTapped);
    }
    
    GetTapped(cards : Card[]) {
        return cards.filter(c => c.IsTapped);
    }

    CanPlayCardWithLands(lands: Card[], card : Card) {
        var untapped = this.GetUntapped(lands);
        
    }
}

export default new CardsHelper();