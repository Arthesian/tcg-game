/**
 * Location of the card in the game
 *
 * @export
 * @enum {number}
 */
export enum CardLocation {
    Any, Deck, Hand, BattleField, Graveyard, Exile, Phased, Casting, Viewing
};

/**
 * Main card type
 *
 * @export
 * @enum {number}
 */
export enum CardType {
    Any, Artifact, Land, Creature, Legendary, Enchantment, Instant, Sorcery, Basic
}

/**
 * Operator type for costs and effects for example
 *
 * @export
 * @enum {number}
 */
export enum OperatorType {
    OR, AND, ANDOR
}

/**
 * Different kind of cost types
 *
 * @export
 * @enum {number}
 */
export enum CostType {
    Mana, Sacrifice, Tap, Discard
}

/**
 * Different kind of mana types
 *
 * @export
 * @enum {number}
 */
export enum ManaType {
    Any, Colorless, Forest, Swamp, Plains, Mountain, Island
}

/**
 * Evergreen keywords used for effect resolving
 *
 * @export
 * @enum {number}
 */
export enum Evergreen {
    Vigilance, Trample, Indestructable, Hexproof, Shroud, FirstStrike, DoubleStrike, 
    Unblockable, Horsemanship, Fear, Intimidate, Islandwalk, Plainswalk, Mountainwalk, Swampwalk, Forestwalk,
    Shadow, Flying, Lifelink, Reach
}