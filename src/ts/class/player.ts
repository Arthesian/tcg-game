import * as Helper from './helper';
import Deck from './deck';
import Hand from './hand';
import Board from './board';
import Base from './base';
import Game from './game';

export default class Player extends Base {

    Game : Game;

    Deck : Deck;
    Hand : Hand;
    Board : Board;

    Mulligans: number = 0;
    LifeTotal : number = 20;

    /**
     * Creates an instance of Player.
     * 
     * @param {*} Game
     * @memberof Player
     */
    constructor(Game) {
        super();

        this.Game = Game;

        if(!this.Game.FreeMulligan) {
            this.Mulligans = 1;
        }
    }

    /**
     * Draw hand with a certain size
     *
     * @memberof Player
     */
    DrawHand = (size) => {
        this.Hand.Draw(size);

        this.ShowKeepHandWindow();
    }

    /**
     * Show a window to confirm to keep hand or mulligan ( player chooses )
     *
     * @memberof Player
     */
    ShowKeepHandWindow() {
        var result = confirm('Keep this hand?');

        if(result) {
            this.KeepHand();
        }else{
            this.Mulligan();
        }
    }

    /**
     * Keep the hand -> player is ready to start game
     *
     * @memberof Player
     */
    KeepHand = () => {
        // Start the game when all players keep hands
        this.triggerEvent("player:ready", [this]);
    }

    /**
     * Redraw hand ( with 1 less card for each mulligan )
     *
     * @memberof Player
     */
    Mulligan = () => {
        // Put cards from hand in Deck
        this.Deck.AddCards(this.Hand.Cards);
        this.Hand.Cards = [];

        // Shuffle deck
        this.Deck.Shuffle();

        // Draw hand with less cards
        this.DrawHand(7 - this.Mulligans++);
    }

    /**
     * Start the turn
     *
     * @param {*} isFirstTurn
     * @memberof Player
     */
    StartTurn(isFirstTurn) {
        this.triggerEvent("player:startturn", [this]);

        
    }

    /**
     * Untap step
     *
     * @returns
     * @memberof Player
     */
    UntapStep(){
        return new Promise(resolve => {

            Helper.Cards.UnTap(this.Board.Cards).then((cards) => {
                resolve(cards);
            });


            this.triggerEvent("player:untap", [this]);
        });
    }

    /**
     * Upkeep step
     *
     * @memberof Player
     */
    UpKeepStep(){

        this.triggerEvent("player:upkeep", [this]);
    }

    /**
     * Draw step
     *
     * @memberof Player
     */
    DrawStep() {
        // callback to firstMainPhase
        this.triggerEvent("player:drawstep", [this]);
    }

    /**
     * Start first main phase
     *
     * @memberof Player
     */
    FirstMainPhase() {
        // callback to declareAttackPhase
        this.triggerEvent("player:main", [this]);
    }

    /**
     * Start declare attack phase
     *
     * @memberof Player
     */
    DeclareAttackPhase() {
        // callback to ENEMY declre block phase
        this.triggerEvent("player:attack", [this]);
    }

    /**
     * Start declare block phase ( attacked players only )
     *
     * @memberof Player
     */
    DeclareBlockPhase(){
        // callback to resolve combat
        this.triggerEvent("player:block", [this]);
    }

    /**
     * Resolve combat phase
     *
     * @memberof Player
     */
    ResolveCombat() {
        // callback to this.secondMainPhase();
        this.triggerEvent("player:block", [this]);
    }

    /**
     * Start the second main phase
     *
     * @memberof Player
     */
    SecondMainPhase(){
        // callback to this.EndStep();
        this.triggerEvent("player:main", [this]);
    }

    /**
     * End step of turn
     *
     * @memberof Player
     */
    EndStep(){
        // callback to this.EndTurn();
        this.triggerEvent("player:endstep", [this]);
    }

    /**
     * End the turn at the end
     *
     * @memberof Player
     */
    EndTurn() {
        this.triggerEvent("player:endturn", [this]);
    }

    /**
     * Lose life
     *
     * @param {*} amount
     * @memberof Player
     */
    LoseLife(amount) {
        this.LifeTotal -= amount;

        if(this.LifeTotal <= 0) {
            this.Game.EndGame();
        }

        this.triggerEvent("player:loselife", [this, amount]);
    }

    /**
     * Gain life
     *
     * @param {*} amount
     * @memberof Player
     */
    GainLife(amount) {
        this.LifeTotal += amount;

        this.triggerEvent("player:gainlife", [this, amount]);
    }
}