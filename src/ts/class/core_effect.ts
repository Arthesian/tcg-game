import * as Cost from './costs';
import Effect_Handler from './effect_handler';

export default class Core_Effect {

    Target : any;

    readonly Count : number;
    readonly Restrictions : any; // object with key : val ( ex. 'Player' : [0 = any, 1 = self, 2 = ally, 3 = enemy] 'CardType' : 'Permanent', 'Type' : 'Legendary Creature', 'SubType' : 'Merfolk' )

    /**
     * Resolve an core effect
     *  
     * @returns Promise of the resolve
     * @memberof Core_Effect
     */
    Resolve() {
        return new Promise(resolve => {

            Effect_Handler.Handle(this).then(() => {

                resolve();
            });
        });
    }
}