import Player from './player'
import * as Helper from './helper'
import Base from './base';

export default class Game extends Base {
    Players : Player[];
    
    CurrentPlayer : Player;
    NextPlayer : Player;

    FreeMulligan : Boolean = false;

    Turn: 0;

    /**
     * Creates an instance of Game.
     * @memberof Game
     */
    constructor() {
        super();
    }

    /**
     * Create a game with players and a starting life
     *
     * @param {Player[]} players
     * @param {number} lifeTotal
     * @memberof Game
     */
    CreateGame(players : Player[], lifeTotal : number) {
        
        // Set players, and set lifetotal
        this.Players = players;
        this.Players.forEach(p => p.LifeTotal = lifeTotal);
    }

    /**
     * Remove a player from the game ( lost )
     *
     * @param {*} player
     * @returns
     * @memberof Game
     */
    RemovePlayer(player) {
        return new Promise(resolve => {
            this.Subtract('Players', player);

            // TODO:: animate player death :P
            setTimeout(() => {
                resolve();
            }, 1000);
        });
    }

    /**
     * Reset the game ( reset decks, lifetotal etc. )
     *
     * @memberof Game
     */
    ResetGame() {
        // Reset all players, reset life totals, clear boards and decks?
    }

    /**
     * Start a new game
     *
     * @memberof Game
     */
    BeginGame() {

        this.ResetGame();

        this.PlayersDrawHand();

        $(this.EventBus).on("player:ready", (event, player : Player) => { this.OnPlayerReady(player); });
    }

    /**
     * Players draw their first hand
     *
     * @memberof Game
     */
    PlayersDrawHand() {
        for(var i in this.Players) {
            var player = this.Players[i];
            player.DrawHand(7);
        }
    }

    /**
     * Track the number of players that are redy to start the game
     *
     * @private
     * @type {Player[]}
     * @memberof Game
     */
    private ReadyPlayers : Player[] = [];
    private OnPlayerReady(player : Player) {
        this.ReadyPlayers.push(player);

        if(this.ReadyPlayers.length == this.Players.length) {
            this.ReadyPlayers = [];
            this.StartNextTurn();
        }
    }

    /**
     * Get a random player in the game
     *
     * @returns
     * @memberof Game
     */
    GetRandomPlayer() {
        return this.Players[Math.floor(Math.random() * this.Players.length) - 1];
    }

    /**
     * Get the next player ( turn )
     *
     * @returns
     * @memberof Game
     */
    GetNextPlayer() {
        var index = this.Players.indexOf(this.CurrentPlayer);
        
        index++;

        if(this.Players.length == index){
            index = 0;
        }

        return this.Players[index];
    }

    /**
     * Start the next turn
     *
     * @memberof Game
     */
    StartNextTurn() {

        this.Turn++;

        if(!this.CurrentPlayer) {
            this.CurrentPlayer = this.GetRandomPlayer();
        }else{
            this.CurrentPlayer = this.NextPlayer;
        }
        this.NextPlayer = this.GetNextPlayer();

        $(this.CurrentPlayer.EventBus).one("player:endturn", () => { this.StartNextTurn(); });

        this.CurrentPlayer.StartTurn(this.Turn === 1);
    }

    /**
     * Show a timer before a player's turn step is missed
     *
     * @private
     * @memberof Game
     */
    private ReactTimeout;
    ShowReactTimer(seconds, callback) {
        
        if(this.ReactTimeout) { clearTimeout(this.ReactTimeout); }
        
        this.ReactTimeout = setTimeout(() => {
            callback();
        }, seconds * 1000);
    }

    /**
     * End the game ( one player wins )
     *
     * @memberof Game
     */
    EndGame() {
        this.triggerEvent("game:end");
    }
}