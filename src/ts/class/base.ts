/// <reference path="./../libs/jquery.d.ts" />
/// <reference path="../libs/easeljs/index.d.ts" />

import eventBus from './eventbus';

export default class Base {
    EventBus;
    Container : createjs.Container;

    /**
     * Creates an instance of Base.
     * @memberof Base
     */
    constructor() {
        this.EventBus = eventBus.instance;
    }

    /**
     * Trigger an event ( property change for example )
     *
     * @param {string} eventName
     * @param {*} [args]
     * @memberof Base
     */
    triggerEvent(eventName : string, args? : any){ 

        args = args || this;

        $(this.EventBus).trigger(eventName, args);
    }

    /**
     * Set a property value
     *
     * @param {string} property
     * @param {*} value
     * @memberof Base
     */
    setValue(property : string, value : any) {
        if(this[property]) {
            var oldValue = this[property];

            this.triggerEvent(this.constructor.name + ':propertychanged', [this, { oldValue : oldValue, newValue : value}]);
        }

        this[property] = value;
    }

    /**
     * Get a property value
     *
     * @param {string} property
     * @returns
     * @memberof Base
     */
    getValue(property : string) {
        if(this[property]) { return this[property]; }

        return null;
    }

    /**
     * Add an instance or up an value
     *
     * @param {string} property
     * @param {*} value
     * @memberof Base
     */
    Add(property : string, value : any){

        var currentValue = this[property];

        if(Array.isArray(currentValue)) {

            if(Array.isArray(value)) {
                this.setValue(property, currentValue.push(...value));
            }else {
                this.setValue(property, currentValue.push(value));
            }
        }else{
            this.setValue(property, currentValue + value);
        }
    }

    /**
     * Remove an instance or lower value
     *
     * @param {string} property
     * @param {*} value
     * @memberof Base
     */
    Subtract(property : string, value : any){
        
        var currentValue = this[property];

        if(Array.isArray(currentValue)) {

            if(Array.isArray(value)) {
                this.setValue(property, currentValue.filter((el) => !value.includes(el)));
            }else {
                this.setValue(property, currentValue.filter((el) => value !== el));
            }
        }else{
            this.setValue(property, currentValue - value);
        }
    }
}