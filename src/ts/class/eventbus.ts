/**
 * Eventbus for sending and listening to events
 *
 * @class EventBus
 */
class EventBus {

    private _instance;

    get instance () {
        if(!this._instance) {
            this._instance = $(document.createElement("object"));
        }

        return this._instance;
    }
}

var eventBus = new EventBus();

// Singleton
export default eventBus;