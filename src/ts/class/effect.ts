import Core_Effect from "./core_effect";

export default class Effect {

    Effects : Core_Effect[];

    /**
     * Resolve main effect of 
     *
     * @returns
     * @memberof Effect
     */
    Resolve() {
        return new Promise(resolve => {
            // Do whatever needs to happen? XD
            this.ResolveCore().then(() => resolve());
        });
    }

    /**
     * Resolve Core effects
     *
     * @private
     * @returns
     * @memberof Effect
     */
    private ResolveCore() {

        var effects = this.Effects.splice(0,0);

        return new Promise(resolve => {
            if(this.Effects.length == 0) { resolve(); }

            var effect = this.Effects.shift();

            effect.Resolve().then(() => this.ResolveCore());
        });
    }
}