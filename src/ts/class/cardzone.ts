import iCardZone from './../interface/cardzone';
import * as Helper from './helper'
import Card from './card';
import Base from './base';

/**
 * Base Card Zone class. Contains functions and methods concerning cards within a zone (hand, deck, graveyard, exile etc.)
 *
 * @export
 * @class CardZone
 * @extends {Base}
 * @implements {CardZone}
 */
export default class CardZone extends Base implements iCardZone {
    
    Cards : Card[];

    /**
     * Creates an instance of CardZone.
     * @memberof CardZone
     */
    constructor() {
        super();

        this.Cards = [];
    }

    /**
     * Check whether this number of cards is available in the zone
     *
     * @param {*} number check if this amound of cards are available in this zone
     * @returns
     * @memberof CardZone
     */
    CanTake(number) {
        return this.GetNumberOfCards() >= number;
    }

    /**
     * Shuffle the cards in this zone
     *
     * @memberof CardZone
     */
    Shuffle() {
        this.Cards = Helper.Cards.Shuffle(this.Cards);

        this.triggerEvent(`${this.constructor.name}:shuffled`, this);

        return this;
    }

    /**
     * Add a card to this zone
     *
     * @param {Card} card to add to this zone
     * @memberof CardZone
     */
    AddCard(card : Card) {
        this.Cards.push(card);

        this.triggerEvent(`${this.constructor.name}:added`, [this, card]);

        return this;
    }

    /**
     * Add an array of cards to this zone
     *
     * @param {Card[]} cards to add to this zone
     * @memberof CardZone
     */
    AddCards(cards : Card[]) {
        cards.forEach(c => this.AddCard(c));

        return this;
    }

    /**
     * Remove a specific card from this zone
     *
     * @param {Card} card to remove from this zone
     * @memberof CardZone
     */
    RemoveCard(card : Card) {
        var i = this.Cards.indexOf(card);
        this.Cards.splice(i, 1);

        this.triggerEvent(`${this.constructor.name}:removed`, [this, card]);

        return this;
    }

    /**
     * Remove an array of cards from this zone
     *
     * @param {Card[]} cards to remove from this zone
     * @memberof CardZone
     */
    RemoveCards(cards : Card[]) {
        cards.forEach(c => this.RemoveCard(c));

        return this;
    }

    /**
     * Get the number of cards in this zone
     *
     * @returns the number of cards in the zone
     * @memberof CardZone
     */
    GetNumberOfCards() {
        return this.Cards.length;
    }
}