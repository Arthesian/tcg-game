/// <reference path="../libs/tweenjs/index.d.ts" />

import * as Enums from './../enums/enums';
import * as Cost from './costs';
import * as Helper from './helper';
import Base from './base';
import Player from './player';

export default class PlayCard extends Base {
    
    // Data properties
    ImageUrl : string;              // Url to the card image
    Artist : string;                // Name of the artist of the picture
    Flavour : string;               // Flavour text of a card, can be empty
    Text : string;                  // Rule text of the card
    
    // Game logic properties
    Owner : Player;
    Controller : Player;
    
    GameLocation : Enums.CardLocation;
    
    PlayCosts : Cost.Costs;         // Supports multiple costs
    ConvertedManaCost : number;     // Converted Mana cost
    Colors : Enums.ManaType[];      // Supports multiple colors
    
    Types : Enums.CardType[];       // Supports multiple types      (ex. 'Artifact Creature')
    SubTypes : string[];            // Supports multiple sub types  (ex. 'Human Rebel')

    Evergreens : Enums.Evergreen[];
    Protections : (Enums.CardType | Enums.ManaType | string)[]; // CardType, Color or SubType

    Enchantments : PlayCard[];      // This card is enchanted by other cards

    Counters : any;                 // Energy, Time, Plus ( +1/+1 ), Min ( -1/-1 ) etc.
    Modifiers : any;                // Is calculated based on Equipments, Instants?, Enchantments -- modifiers for attack, defense, evergreens  

    IsTapped : boolean;
    UnTapSkipTurns : number = 0;

    /**
     * Creates an instance of PlayCard.
     * @memberof PlayCard
     */
    constructor() {
        super();

        this._SetConvertedManaCost();
    }

    /**
     * Set the converted mana cost of the card 
     * based on the mana costs of the card
     *
     * @private
     * @memberof PlayCard
     */
    private _SetConvertedManaCost = () => { 
        var cost = 0;

        this.PlayCosts.Costs.filter(pc => pc.CostType === Enums.CostType.Mana).forEach(m => cost += m.Amount);
        this.ConvertedManaCost = cost; 
    }

    /**
     * Tap this card
     *
     * @returns Promise of the tap event
     * @memberof PlayCard
     */
    Tap() {
        return new Promise(resolve => {
            this.setValue('IsTapped', true);

            createjs.Tween.get(this.Container).to({ rotation : 70 }, 500).call(() => {
                resolve(this);
            });
        });
    }

    /**
     * Untap this card
     *
     * @param {boolean} [force] Optional parameter to force untap the card ( for example : override 'doesn't untap during untap step' )
     * @returns Promise of the untap event
     * @memberof PlayCard
     */
    UnTap(force? : boolean) {
        return new Promise(resolve => {
            if(!force && this.UnTapSkipTurns){
                this.UnTapSkipTurns--;

                resolve(this);
            }else{
                this.UnTapSkipTurns = 0;
    
                this.setValue('IsTapped', false);

                // Animate Untap??
                createjs.Tween.get(this.Container).to({ rotation : 0 }, 500).call(() => {
                    resolve(this);
                });
            }
        });
    }
    
    /**
     * Move this card to a different Card Zone
     *
     * @param {Enums.CardLocation} zone Zone to move the card to
     * @returns Promise of the movement of the card ( animation )
     * @memberof PlayCard
     */
    MoveToZone(zone : Enums.CardLocation) {
        return new Promise(resolve => {
            this.setValue('GameLocation', zone);

            // Animate moving
            createjs.Tween.get(this.Container).to({ x : this.Container.y + 100 }, 500).call(() => {
                resolve(this);
            });
        })
    }

    /**
     * Mark the card as a selectable card
     *
     * @param {boolean} selectable True or false
     * @memberof PlayCard
     */
    SetSelectable(selectable : boolean) {
        //this.Container.addFilter(nice filter);
    }
};

export { PlayCard as Card };