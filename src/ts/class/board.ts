import Card from './card'
import * as Helper from './helper';
import Base from './base';
import CardZone from './cardzone';
import { CardType } from '../enums/enums';

export default class Board extends CardZone {
    Cards : Card[];

    // Specific card type lookup on the field
    GetType = (type : CardType) => { return this.Cards.filter((c) => c.Types.filter((t) => { t === type }));}
    GetLands = () => { return this.GetType(CardType.Land); };
    GetCreatures = () => { return this.GetType(CardType.Creature); };
    GetArtifacts = () => { return this.GetType(CardType.Artifact); };
    GetEnchantments = () => { return this.GetType(CardType.Enchantment); };

    /**
     * Untap all cards on the board
     */
    UntapAll() {
        Helper.Cards.UnTap(this.Cards);

        this.triggerEvent("board:untap", this.Cards);
    }

    /**
     * Untap all cards of a specific type
     * 
     * @param type CardType
     */
    UntapType(type : CardType) {
        var cards = this.GetType(type);
        Helper.Cards.UnTap(cards);

        this.triggerEvent("board:untap", cards)
        this.triggerEvent("board:untap" + type, cards);
    }

    /**
     * Untap all lands on the field
     */
    UntapLands() {
        this.UntapType(CardType.Land);
    }

    /**
     * Untap all creatures
     */
    UntapCreatures() {
        this.UntapType(CardType.Creature);
    }
}