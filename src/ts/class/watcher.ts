// object.watch
interface Object {
    watch : (prop, handler) => void;
    unwatch : (prop) => void;
}

if (!Object.prototype.watch)
{
    Object.prototype.watch = function (prop, handler)
    {
        var val = this[prop],
        getter = function ()
        {
            return val;
        },
        setter = function (newval) {
            return val = handler.call(this, prop, val, newval);
        };
        if (delete this[prop]) { // can't watch constants
            if (Object.defineProperty) { // ECMAScript 5
                Object.defineProperty(this, prop, {
                   get: getter,
                   set: setter
                });
            }
        }
    };
}
 
// object.unwatch
if (!Object.prototype.unwatch)
{
    Object.prototype.unwatch = function (prop) {
        var val = this[prop];
        delete this[prop]; // remove accessors
        this[prop] = val;
    };
}