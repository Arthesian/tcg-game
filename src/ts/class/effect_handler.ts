import Core_Effect from "./core_effect";
import Card from "./card";
import card_selector from "./card_selector";

class Effect_Handler {

    /**
     * Handle the core effect
     *
     * @param {Core_Effect} effect
     * @returns
     * @memberof Effect_Handler
     */
    Handle(effect : Core_Effect) {
        
        var targets : Card[];

        return new Promise(resolve => {

            if(effect.Restrictions) {
                var availableCards = card_selector.GetCardsWithConditions(targets, effect.Restrictions);

                if(!availableCards || availableCards.length == 0) {
                    resolve();
                }
            }
        });
    }

    /**
     * Select targets (????)
     *
     * @private
     * @memberof Effect_Handler
     */
    private Select_Targets() {

    }

    /**
     * Moce cards (?????)
     *
     * @private
     * @memberof Effect_Handler
     */
    private Move_Targets() {

    }
}

var effect_handler = new Effect_Handler();

export default effect_handler;