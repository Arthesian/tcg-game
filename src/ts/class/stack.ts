import Base from "./base";
import Effect from "./effect";

export default class Stack extends Base {

    _stack : Effect[];

    /**
     * Check whether the stack is empty
     *
     * @returns
     * @memberof Stack
     */
    IsEmpty() {
        return !!this._stack.length;
    }

    /**
     * Add an effect to the stack
     *
     * @param {*} effect
     * @memberof Stack
     */
    AddToStack(effect) {
        this._stack.push(effect);
    }

    /**
     * Resolve the stack FILO ( First-In, Last-Out )
     *
     * @returns
     * @memberof Stack
     */
    ResolveStack() {

        return new Promise(resolve => {
            this.ResolveTopStackEffect().then(() => resolve());
        });

    }

    /**
     * Resolve the top effect
     * 
     * @private
     * @returns
     * @memberof Stack
     */
    private ResolveTopStackEffect() {
        return new Promise(resolve => {
            if(!this._stack.length) { resolve(); }

            var effect = this._stack.pop();

            effect.Resolve().then(() => this.ResolveTopStackEffect());
        });
    }

}