import * as Enums from './../enums/enums';
import * as Helper from './helper';
import Player from './player';
import Card from './card';

/**
 * Costs object with operator
 *
 * @export
 * @class Costs
 */
export class Costs {
    CostsOperator : Enums.OperatorType 
    Costs : BaseCost[]
}

/**
 * Base Cost of an ability or card
 *
 * @export
 * @class BaseCost
 */
export class BaseCost {
    CostType : Enums.CostType
    CardType : Enums.CardType
    IsPayed : Boolean
    Forced : Boolean
    Player : Player
    Amount : number
    Restrictions : any
}

/**
 * Definition of Mana cost
 *
 * @export
 * @class ManaCost
 * @extends {BaseCost}
 */
export class ManaCost extends BaseCost {
    CostType : Enums.CostType.Mana
    Colors : Enums.ManaType[] // B/W = Black or White, B = Black
}

/**
 * Definition of a sacrifice cost
 *
 * @export
 * @class SacrificeCost
 * @extends {BaseCost}
 */
export class SacrificeCost extends BaseCost {
    CostType : Enums.CostType.Sacrifice
}

/**
 * Definiton of Discard costs
 *
 * @export
 * @class DiscardCost
 * @extends {BaseCost}
 */
export class DiscardCost extends BaseCost {
    CostType : Enums.CostType.Discard
    CardSubType : string

    GetValidOptions() {
        if(this.CardSubType) {
            return Helper.Cards.GetSubType(this.Player.Hand.Cards, this.CardSubType);
        }else if (this.CardType) {
            return Helper.Cards.GetType(this.Player.Hand.Cards, this.CardType);
        }
    }
}

/**
 * Definition of tap cost
 *
 * @export
 * @class TapCost
 * @extends {BaseCost}
 */
export class TapCost extends BaseCost { 
    CostType : Enums.CostType.Tap
    CardSubType : string
    Card : Card

    GetValidOptions() {
        if(this.Card) {
            return this.Card;
        }else if(this.CardSubType) {
            return Helper.Cards.GetSubType(this.Player.Hand.Cards, this.CardSubType);
        }else if (this.CardType) {
            return Helper.Cards.GetType(this.Player.Hand.Cards, this.CardType);
        }
    }
}