import * as Enums from './../enums/enums';
import * as Cost from './costs';
import * as Helper from './helper'
import Card from './card';
import CardZone from './cardzone';

export default class Deck extends CardZone {

    SideBoard : Card[];

    Name : string;
    Creator : string;

    /**
     * Creates an instance of Deck.
     * @memberof Deck
     */
    constructor() {
        super();
    }

    /**
     * Put an card on bottom of the deck
     *
     * @param {Card} card
     * @memberof Deck
     */
    PutCardOnBottom(card : Card) {
        this.Cards.push(card);

        this.triggerEvent("deck:cardonbottom", [this, card]);
    }

    /**
     * Put multiple cards on bottom of the deck
     *
     * @param {Card[]} cards
     * @memberof Deck
     */
    PutCardsOnBottom(cards : Card[]) {
        cards.forEach(c => this.PutCardOnBottom(c));
    }

    /**
     * Put card on top of the deck
     *
     * @param {Card} card
     * @memberof Deck
     */
    putCardOnTop(card : Card) {
        this.Cards.splice(0,0,card);

        this.triggerEvent("deck:cardontop", [this, card]);
    }

    /**
     * Put multiple cards on top of the deck
     *
     * @param {Card[]} cards
     * @memberof Deck
     */
    PutCardsOnTop(cards : Card[]) {
        cards.forEach(c => this.putCardOnTop(c));
    }

    /**
     * View the top cards
     *
     * @param {*} number of cards to view
     * @returns {Card[]} Cards that are selected to view
     * @memberof Deck
     */
    ViewTopCards(number) {
        if(this.CanTake(number)) {
            return this.Cards.slice(0, number);
        }else{
            return this.Cards.slice(0, this.Cards.length - 1); // View maximum amount of cards
        }
    }

    /**
     * Draw cards from the deck until a certain color card is drawn
     *
     * @param {Enums.ManaType} color Color of the card when to stop
     * @returns {Card[]} Array of cards that where drawn
     * @memberof Deck
     */
    GetUntilColor(color: Enums.ManaType) {
        var cards : Card[] = [];

        var card : Card;

        do {
            card = this.GetTopCard();

            cards.push(card);

        }while(!card.Colors.some(c => c == color) && this.CanTake(1));

        return cards;
    }

    /**
     * Draw cards from the deck until a certain type is drawn
     *
     * @param {Enums.CardType} type
     * @returns
     * @memberof Deck
     */
    GetUntilType(type: Enums.CardType) {
        var cards : Card[] = [];

        var card : Card;

        do {
            card = this.GetTopCard();

            cards.push(card);

        }while(!card.Types.some(t => t == type) && this.CanTake(1));

        return cards;
    }

    /**
     * Get the top card of the deck
     *
     * @returns {Card} Top card on top of the deck
     * @memberof Deck
     */
    GetTopCard() {
        var card = this.GetTopCards(1)[0];
        
        if(card) { return card; }
    }

    /**
     * Draw top X cards of the deck
     *
     * @param {*} number Number of cards to draw
     * @default 1 card
     * @returns {Card[] | null} Return the drawn cards, or null if the deck is empty
     * @memberof Deck
     */
    GetTopCards(number = 1) {
        if(this.CanTake(number)){
            var cards = this.Cards.splice(0, number);

            this.triggerEvent("deck:draw", [this, cards]);

            return cards;
        }else{

            this.triggerEvent('deck:empty', [this]);
        }
    }
}