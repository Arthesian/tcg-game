import { Card } from "./card";
import Base from "./base";
import CardZone from "./cardzone";

class Card_Selector extends Base {

    /**
     * Select all cards that meet a certain condition in a Card Zone
     * 
     * @param cards Cards to look into
     * @param restrictions Restrictions that apply to the selection
     */
    GetCardsWithConditions(cards : Card[], restrictions : any) {

        var filteredCards = cards.filter(card => {

            var match = Object.keys(restrictions).every(key => {

                if(card[key]) {

                    let valuesString : string = card[key];

                    let values = valuesString.split(',');

                    var exists = values.some(v => v == card[key]);

                    return exists;
                }

                return false;
            });

            return match;
        });

        return filteredCards;
    }

    /**
     * Player needs to choose from available options
     * 
     * @param availableOptions Cards that are options
     * @param number Number of cards that need to be picked
     * @param upTo Is it up-to the number, or should it be equal to the number
     */
    PlayerSelectCards(availableOptions : Card[], number : number, upTo : boolean) {
        
        return new Promise(resolve => {
       
            // Special combination for 'ALL'
            if (number === -1 && !upTo) { 
                return availableOptions; 
            
            // Special combination for 'Any' ( = up to : all )
            }else if(number === -1 && upTo) {
                this.ChooseCards(availableOptions).then((cards) => {
                    return cards;
                });
            }
        });
    }

    /**
     * @private
     * 
     * Function to visually let the user choose cards
     * 
     * @param cards Cards to choose from
     */
    private ChooseCards(cards : Card[]) {

        this.SetSelectableCards(cards);

        return new Promise(resolve => {

            $(this.EventBus).one(this.EventBus, 'cardsSelectedButton:click', (selectedCards : Card[]) => {
                
                this.SetSelectableCards(cards, true);
                
                resolve(selectedCards);
            });
        });
    }

    /**
     * @private
     * 
     * Function to mark cards as selected
     * 
     * @param cards Cards to mark as selected
     * @param unselect Select or deselect
     */
    private SetSelectableCards(cards : Card[], unselect? : boolean) { 
        cards.forEach(card => {
            card.SetSelectable(!unselect);
        });
    }
}

// Return singleton of the Card Selector
var card_selector = new Card_Selector();

export default card_selector;