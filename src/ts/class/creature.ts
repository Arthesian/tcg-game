import Card from  './card';
import * as Helper from './helper';
import { Evergreen, ManaType, CardType } from '../enums/enums';

export class Creature extends Card {
    public Strength : number;
    public Toughness : number;

    public Equipments : Card[];

    /**
     * Function to handle the attack move
     *
     * @memberof Creature
     */
    Attack() {

        if(!this.Evergreens.includes(Evergreen.Vigilance)) {
            this.Tap();
        }

        this.triggerEvent("creature:attack", [this]);
    }

    /**
     * Check whether this creature can block an attacking creature
     *
     * @param {Creature} attacker
     * @returns {boolean} whether the creature can be blocked
     * @memberof Creature
     */
    CanBlock(attacker : Creature) {
        
        if(this.IsTapped) { return false; }
        
        // Check unblockable
        if(attacker.Evergreens.includes(Evergreen.Unblockable)) { return false; }

        // Check landwalks
        var lands = this.Controller.Board.GetLands();
        if(attacker.Evergreens.includes(Evergreen.Plainswalk) && Helper.Cards.GetBasicLandsOfType(lands, ManaType.Plains).length > 0) { return false; }
        if(attacker.Evergreens.includes(Evergreen.Islandwalk) && Helper.Cards.GetBasicLandsOfType(lands, ManaType.Island).length > 0) { return false; }
        if(attacker.Evergreens.includes(Evergreen.Swampwalk) && Helper.Cards.GetBasicLandsOfType(lands, ManaType.Swamp).length > 0) { return false; }
        if(attacker.Evergreens.includes(Evergreen.Mountainwalk) && Helper.Cards.GetBasicLandsOfType(lands, ManaType.Mountain).length > 0) { return false; }
        if(attacker.Evergreens.includes(Evergreen.Forestwalk) && Helper.Cards.GetBasicLandsOfType(lands, ManaType.Forest).length > 0) { return false; }

        // Check protection for color | cardtype | subtype  ( ex. protection from blue, creatures or merfolk )
        if(this.Colors.some(c => attacker.Protections.includes(c)) || 
            this.Types.some(t => attacker.Protections.includes(t)) ||
            this.SubTypes.some(st => attacker.Protections.includes(st))) { return false; }

        // Check fear
        if(attacker.Evergreens.includes(Evergreen.Fear) && (!this.Colors.includes(ManaType.Colorless) && !this.Colors.includes(ManaType.Swamp))) { return false; }

        // Check flying
        if(attacker.Evergreens.includes(Evergreen.Flying) && (!this.Evergreens.includes(Evergreen.Flying) && !this.Evergreens.includes(Evergreen.Reach))) { return false; } 

        // Horsemanship
        if(attacker.Evergreens.includes(Evergreen.Horsemanship) && !this.Evergreens.includes(Evergreen.Horsemanship)) { return false; }
        
        // Shadow
        if(attacker.Evergreens.includes(Evergreen.Shadow) && !this.Evergreens.includes(Evergreen.Shadow)) { return false; }

        // Todo : Intimidate ( 2+ blockers )

        // Can block -- I guess...
        return true;
    }

    /**
     * Function to handle the block of this creature
     *
     * @param {Creature} creature
     * @memberof Creature
     */
    Block(creature : Creature) {
        this.triggerEvent("creature:block", [this, creature]);
    }

    /**
     * Function to call when this creature dies
     *
     * @memberof Creature
     */
    Die() {
        this.triggerEvent("creature:die", [this]);
    }
}