import * as Enums from './../enums/enums';
import * as Helper from './helper';
import * as Cost from './costs';
import Card from './card';
import Player from './player';
import Base from './base';
import CardZone from './cardzone';

export default class Hand extends CardZone {
    
    Player : Player;
    
    Limit : number = 7;
    LandPlayLimit : number = 1;
    LandsPlayedThisTurn : number = 0;

    /**
     * Creates an instance of Hand.
     * 
     * @param {Player} player
     * @memberof Hand
     */
    constructor(player : Player){
        super();

        this.Player = player;
        this.Cards = [];
    }

    /**
     * Draw number of cards
     *
     * @param {number} [number=1]
     * @memberof Hand
     */
    Draw(number : number = 1) {

        var drew = this.Player.Deck.GetTopCards(number);

        this.Add('Cards', drew);
    }

    /**
     * Discard a random card from the hand
     *
     * @memberof Hand
     */
    DiscardRandom() {
        var i = Math.round(Math.random() * (this.Cards.length - 1));

        this.Discard(this.Cards[i]);
    }

    /**
     * Discard a specific card from hand
     *
     * @param {Card} card
     * @memberof Hand
     */
    Discard(card : Card){ 
        this.Subtract('Cards', card);
    }

    /**
     * Check whether the card can be played ( on hover )
     *
     * @param {Card} card
     * @returns
     * @memberof Hand
     */
    CanPlay(card : Card){
        if(Helper.Card.IsLand(card)){
            if(this.LandsPlayedThisTurn < this.LandPlayLimit) {
                return true;
            }
        }else {
            // Check costs of card, and see if costs can be pa
        }
    }

    /**
     * Choose a card to play
     *
     * @param {Card} card
     * @memberof Hand
     */
    Play(card : Card) {
        if(Helper.Card.IsLand(card)){
            this.LandsPlayedThisTurn++;
        }

        this.triggerEvent("hand:play", [card]);
    }
}