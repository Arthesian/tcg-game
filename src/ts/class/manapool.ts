import Base from "./base";
import { ManaType, CostType, OperatorType } from "../enums/enums";
import Card from "./card";
import { Costs, BaseCost, ManaCost } from "./costs";

export default class Manapool extends Base {

    private _pool : ManaType[];

    /**
     * Check whether a certain type of mana is in the pool
     *
     * @param {ManaType} mana
     * @returns
     * @memberof Manapool
     */
    HasMana(mana : ManaType) {

        if(mana === ManaType.Any) { return this._pool.length > 0 }

        return this._pool.some(m => m === mana);
    }

    /**
     * Add mana to your mana pool
     *
     * @param {ManaType} mana
     * @param {number} [amount]
     * @memberof Manapool
     */
    AddMana(mana : ManaType, amount? : number) {
        amount = amount || 1;

        for(var x = 0; x < amount; x++) {
            this._pool.push(mana);
        }
    }

    /**
     * Remove ( array of ) mana from mana pool
     *
     * @param {ManaType[]} mana
     * @memberof Manapool
     */
    RemoveManaMultiple(mana : ManaType[]) {
        var paid = false;

        var index = 0;

        while(!paid) {
            var color = mana[index];

            if(this.HasMana(color)) {
                this.RemoveMana(color);
                paid = true;
            }
        }
    }

    /**
     * Get the type of mana which is most frequent in the pool ( to pay for 'any' )
     *
     * @returns
     * @memberof Manapool
     */
    GetMostManaInPool() {
        return this._pool.sort((a, b) =>
                this._pool.filter(v => v === a).length - this._pool.filter(v => v === b).length
        ).pop();
    }

    /**
     * Remove single mana from the pool
     *
     * @param {ManaType} mana
     * @memberof Manapool
     */
    RemoveMana(mana : ManaType) {
        
        var manaToUse;
        var manaIndex = 0;
        
        if(mana != ManaType.Any) {
            while(!manaToUse) {
                if(this._pool[manaIndex] == mana) {
                    manaToUse = this._pool[manaIndex];
                    break;
                }
                manaIndex++;
            }
        }else {
            if(this.HasMana(ManaType.Colorless)) {
                this.RemoveMana(ManaType.Colorless);
            }else {
                var mostManaType = this.GetMostManaInPool();
                this.RemoveMana(mostManaType);
            }
        }
    }

    /**
     * Check whether the card's playcost can be paid with the current manapool
     *
     * @param {Card} card
     * @returns
     * @memberof Manapool
     */
    CanPayCard(card : Card) {
        var costs = card.PlayCosts;

        return this.CanPayCosts(costs);
    }

    /**
     * Check whether a certain activation cost can be paid
     *
     * @param {Costs} costs
     * @returns
     * @memberof Manapool
     */
    CanPayCosts(costs : Costs) {
        var manaCosts = costs.Costs.filter(c => c.CostType === CostType.Mana) as ManaCost[];

        if(costs.CostsOperator == OperatorType.AND) {

            manaCosts.forEach(mc => {
                if(!this.CanPayCost(mc)) {
                    return false;
                }
            });

            return true;

        }else {

            manaCosts.forEach(mc => {
                if(this.CanPayCost(mc)) {
                    return true;
                }
            });

            return false;
        }
    }

    /**
     * Check whether a certain mana cose can be paid
     *
     * @param {ManaCost} cost
     * @returns
     * @memberof Manapool
     */
    CanPayCost(cost : ManaCost) {
        var manaInPool = this._pool.filter(mt => cost.Colors.includes(mt));

        if (cost.Amount <= manaInPool.length) { return true; }

        return false;
    }

    /**
     * Pay a mana cost
     *
     * @param {ManaCost} cost
     * @memberof Manapool
     */
    PayCost(cost : ManaCost) {
        
        for(var x = 0; x < cost.Amount; x++) {
            
            this.RemoveManaMultiple(cost.Colors);
        }
    }
}